#!/bin/bash
if [ -z "$1" ]; then
  echo "No out_file!"
  exit 1
fi
out_file="$1"
job_name="$2"

cat > "$out_file" <<EOF
image: debian:buster-slim

include: 'template.yml'

$job_name:
  stage: build
  script: echo 'Do nothing'
EOF



